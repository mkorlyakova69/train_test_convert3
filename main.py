# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import numpy as np
import matplotlib.pyplot as plt

import torch
from model import FaceModel
from facenet_pytorch import MTCNN, InceptionResnetV1
import config
import generator
import model
from torch.utils.data import DataLoader



if torch.cuda.is_available():
    device = "cuda:0"
else:
    device = "cpu"
print(device)
device = torch.device(device)

path_im = '/home/mariya/PycharmProjects/overcom/faceid/image/nina/users'


detector = MTCNN(keep_all=True, device=config.FRAME_PROCESSOR_INFERENCE_DEVICE)
encoder = InceptionResnetV1(pretrained='vggface2').eval()


gen_train = generator.CustomImageDataset('train_annot.csv',path_im,transform = generator.transform_train, pretrain = encoder, device=device)
train_dataloader = DataLoader(gen_train, batch_size=2, shuffle=True)

try:
    gen_test = generator.CustomImageDataset('test_annot.csv',path_im,transform = generator.transform_train,  device=device)
    test_dataloader = DataLoader(gen_test, batch_size=2, shuffle=True)

except:
    test_dataloader = []

tinymodel = FaceModel( encoder.to(device))
#tinymodel.load_state_dict(torch.load('model_weights_0_49.pth'))
tinymodel.to(device).eval()


criterion = torch.nn.CrossEntropyLoss()# MSELoss()#.
optimizer = torch.optim.Adam(tinymodel.linear1.parameters(), lr=0.01)#100 0.01, 200 0.0051
lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=200,  gamma=0.5)
# Press the green button in the gutter to run the script.
h_test,h = [],[]

for i in range(1000):
    loss_test, loss_train = model.train_one_epoch(tinymodel, optimizer, criterion, train_dataloader , test_dataloader , device,
                                            i, print_freq=0)
    h += [loss_train]
    h_test += [loss_test]

    lr_scheduler.step()

plt.plot(h, label='train')
plt.plot(h_test, label='test')


if __name__ == '__main__':
    pass


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
