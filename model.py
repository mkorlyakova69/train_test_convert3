import numpy as np
import torch
'['

class FaceModel(torch.nn.Module):

    def __init__(self,pretrain):
        super(FaceModel, self).__init__()
        self.pretrain = pretrain
        self.linear1 = torch.nn.Linear(512, 1)
        self.sigm = torch.nn.Sigmoid()

    def forward(self, x):
        print(x.shape)
        x = self.linear1(x)
        x = self.sigm(x)
        return x

    def predict(self, x):
        x1, x2 = x
        x1r = self.pretrain(x1)


        x = torch.abs(x2 - x1r)

        x = self.linear1(x)
        x = self.sigm(x)
        return x


def evaluate(model, data_loader_test, device='cuda'):
    running_loss_test = 0.0
    i = 0
    for xy in data_loader_test:
        inputs, labels = xy

        outputs = model.forward(inputs.float())
        # print(outputs.shape, labels.shape)

        loss = torch.mean((outputs.float() - labels.float()) ** 2)  # criterion(outputs.float(), labels.float())
        # print(i,' : ',loss," : ",outputs[:5],labels[:5])

        # print statistics
        running_loss_test += loss.item()
        i += 1
    return running_loss_test / i


def train_one_epoch(model, optimizer, criterion, data_loader, data_loader_test , device = 'cuda', epoch=1, print_freq=1):
    running_loss = 0.0
    i = 0
    model.to(device).train()
    for xy in data_loader:
        inputs, labels = xy

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = model.forward(inputs.float())
        # print(outputs.shape, labels.shape)

        loss = torch.mean((outputs.float() - labels.float()) ** 2)  # criterion(outputs.float(), labels.float())
        print(i,' : ',loss," : ",outputs[:5],labels[:5])
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.item()
        i += 1
    if print_freq:  # print every print_freq mini-batches
        print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss / i:.3f}')
        # print(outputs[:5],labels[:5])
    running_loss = running_loss / i
    running_loss_test = evaluate(model, data_loader_test, device=device)

    return running_loss_test, running_loss