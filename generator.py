import numpy as np
import torch
import torch
from torch.utils.data import Dataset
from torchvision import datasets

import matplotlib.pyplot as plt

import os
import pandas as pd
from torchvision.io import read_image
import torchvision.transforms as transforms


img_path_to_file = ''


# IM_SHAPE = [256,256]
IM_SHAPE = [128,128]

transform_train = transforms.Compose(
    [#transforms.ToTensor(),

    transforms.RandomChoice([
     transforms.GaussianBlur(5,sigma=(0.1,2.0)),
     transforms.RandomRotation(10,interpolation=transforms.InterpolationMode.BILINEAR),
    transforms.ColorJitter(brightness=0.30,contrast=0.150,saturation=0.10,hue=0.3)]),
        transforms.RandomEqualize(),
    transforms.Resize(IM_SHAPE,interpolation=transforms.InterpolationMode.BILINEAR),

    #transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])

transform_test = transforms.Compose(
    [#transforms.ToTensor(),
    transforms.Resize(IM_SHAPE,interpolation=transforms.InterpolationMode.BILINEAR),
    #transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])

class CustomImageDataset(Dataset):
    def __init__(self, annotations_file, img_dir = '', transform=None, target_transform=None, pretrain = None, im_shape = IM_SHAPE, device = 'cuda', scale_embeding=5):
        self.img_labels = pd.read_csv(annotations_file,index_col=0)
        self.img_dir = img_dir
        self.transform = transform
        self.target_transform = target_transform
        self.im_shape = im_shape
        self.encoding = pretrain
        self.scale_embeding = scale_embeding
        self.device =  device

    def __len__(self):
        return len(self.img_labels)

    def __getitem__(self, idx):
        img_path1 = os.path.join(self.img_dir, self.img_labels.iloc[idx, 0])
        img_path2 = os.path.join(self.img_dir, self.img_labels.iloc[idx, 2])
        image1 = read_image(img_path1)
        image2 = read_image(img_path2)
        #print(image1.shape)

        label = torch.tensor(int(self.img_labels.iloc[idx, 1] == self.img_labels.iloc[idx, 3]))
        #print(self.img_labels.iloc[idx, 1],self.img_labels.iloc[idx, 3])
        if self.transform:
            #print(image1.shape)
            image1 = self.transform(image1)
            image2 = self.transform(image2)

            #print(image1.shape)

            if self.encoding:
                image1 = image1.reshape(-1, 3, self.im_shape[0], self.im_shape[1]) * 1.0
                image2 = image2.reshape(-1, 3, self.im_shape[0], self.im_shape[1]) * 1.0
                #print(image1.shape)
                images = torch.cat([image1, image2], 0).to(self.device)
                print(images.shape,images.dtype)
                embeding = self.encoding(images)
                end_embeding = torch.abs(embeding[0,:] - embeding[1,:]) * self.scale_embeding
            else:
                end_embeding= [image1,image2]
        if self.target_transform:
            label = self.target_transform(label)

        return end_embeding.to(self.device), label.to(self.device)

