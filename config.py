import os

import torch


DEBUG = bool(int(os.environ.get('DEBUG', 1)))

KWARGS_OPEN_API = {
    "docs_url": "/api/docs", "redoc_url": "/api/redoc",
} if DEBUG else {"docs_url": None, "redoc_url": None}


if torch.cuda.is_available():
    FRAME_PROCESSOR_INFERENCE_DEVICE = 'cuda:0'
    INFERENCE_DEVICE = 'cuda'
else:
    INFERENCE_DEVICE = FRAME_PROCESSOR_INFERENCE_DEVICE = 'cpu'
# INFERENCE_DEVICE = ''  # cuda device, i.e. 0 or 0,1,2,3 or cpu


HOPENET_PATH = os.path.join('cv_module', 'models_zoo', 'hopenet', 'hopenet_robust_alpha1.pkl')


not_identified_frames_counter_limit = int(os.environ.get('not_identified_frames_counter_limit', 5))


FLIP_FRAME = True

# for registration procedure
TIME_REGISTRATION_LIMIT = int(os.environ.get('TIME_REGISTRATION_LIMIT', 60))
ONE_FACE_REGISTRATION = bool(int(os.environ.get('ONE_FACE_REGISTRATION', 1)))


# face quality
USE_FACE_QUALITY = bool(int(os.environ.get('USE_FACE_QUALITY', 1)))
LOG_FACE_QUALITY = bool(int(os.environ.get('LOG_FACE_QUALITY', 0)))
LOG_FACE_QUALITY_KEY = os.environ.get('LOG_FACE_QUALITY_KEY', 'head_pose_x')


# add landmarks and confidence from mtcnn to face
ADD_MTCNN_LANDMARKS = bool(int(os.environ.get('ADD_MTCNN_LANDMARKS', 1)))
ADD_MTCNN_CONFIDENCE = bool(int(os.environ.get('ADD_MTCNN_CONFIDENCE', 1)))

# FACENET_THRESHOLD = 1.1 for the best balance between FRR and FMR,
# but FACENET_THRESHOLD = 1.0 mire 'safe' (less FMR)
# FACENET_THRESHOLD = 0.63 for old facenet (encoding by 128 numbers)
FACENET_THRESHOLD = float(os.environ.get('FACENET_THRESHOLD', 0.82))

# Screen
# OUT_FRAME_HEIGHT, OUT_FRAME_WIDTH, OUT_FRAME_CHANNELS = 720, 1280, 3
FACE_WIDTH_ON_SCREEN, FACE_HEIGHT_ON_SCREEN = 160, 160

# border for saved image
BORDER_SIZE = int(os.environ.get('BORDER_USE', 20))

try:
    from local_settings import *
except ImportError:
    pass
